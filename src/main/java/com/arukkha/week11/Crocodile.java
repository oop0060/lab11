package com.arukkha.week11;

public class Crocodile extends Animal implements Swimable,Diveable,Walkable,Crawlable{

    public Crocodile(String name) {
        super(name, 4);
    }
    @Override
    public void swim() {
        System.out.println(this.toString() + "swim.");
        
    }
    @Override
    public void dive() {
        System.out.println(this.toString() + "dive.");
        
    }
    @Override
    public void sleep() {
        System.out.println(this.toString() + "sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + "eat.");

    }
    @Override
    public String toString() {
        return "Crocodile ("+ this.getName() +")";
    }
    @Override
    public void walk() {
        System.out.println(this.toString() + "walk.");
        
    }

    @Override
    public void run() {
        System.out.println(this.toString() + "run.");
        
    }
    @Override
    public void crawl() {
        System.out.println(this.toString() + "crawl.");
        
    }
}
