package com.arukkha.week11;

public interface Drivenable {
    public void drive();
    public void brake();
}
