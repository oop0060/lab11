package com.arukkha.week11;

public class Bus extends Vehicle implements Drivenable{

    public Bus(String name, String engine) {
        super(name, engine);
    }
    @Override
    public void drive() {
        System.out.println(this.toString() + "drive.");
        
    }
    @Override
    public void brake() {
        System.out.println(this.toString() + "brake.");
        
    }
    @Override
    public String toString() {
        return "Bus(" + getName() +")";
    }
}
