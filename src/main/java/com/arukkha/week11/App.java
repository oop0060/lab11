package com.arukkha.week11;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Bird bird1 = new Bird("Tweety");
        Plane Jet1 = new Plane("Airasia" ,"V8turbocharge");
        Superman sup1 = new Superman("clark");
        Human man1 = new Human("Man");
        Bat bat1 = new Bat("JIJI");
        Snake snake1 = new Snake("JOJO");
        Rat rat1 = new Rat("JAJA");
        Dog dog1 = new Dog("JARJAR");
        Cat cat1 = new Cat("ORO");
        Submarine sub1 = new Submarine("Subzero", "Fordiveonly");
        Fish F1 = new Fish("Johnny");
        Crocodile crocky1 = new Crocodile("Junee");
        Bus B1 = new Bus("somebus", "superpowerengine");

        
        Flyable[] flyables = {bird1,Jet1,sup1,bat1};
        for(int i = 0; i<flyables.length;i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();

        }
        Walkable[] walkables = {bird1,sup1,man1,rat1,dog1,cat1,crocky1};
        for(int i = 0; i<flyables.length;i++) {
            walkables[i].walk();
            walkables[i].run();
        }
        Swimable[] swimables = {sup1,man1,dog1,cat1,F1,crocky1};
        for(int i = 0; i<flyables.length;i++) {
            swimables[i].swim();
        }
        Diveable[] diveables = {sup1,man1,crocky1,sub1};
        for(int i = 0; i<flyables.length;i++) {
            diveables[i].dive();
        }
        Crawlable[] crawlables = {sup1,man1,crocky1,snake1};
        for(int i = 0; i<flyables.length;i++) {
            crawlables[i].crawl();
        }
        Drivenable[] drivenables = {Jet1,B1,sub1};
        for(int i = 0; i<flyables.length;i++) {
            drivenables[i].drive();
            drivenables[i].brake();
        }
    }

}
