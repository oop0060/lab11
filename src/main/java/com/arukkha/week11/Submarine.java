package com.arukkha.week11;

public class Submarine extends Vehicle implements Diveable,Drivenable{

    public Submarine(String name, String engine) {
        super(name, engine);
    }
    @Override
    public String toString() {
        return "Submarine(" + getName() +")";
    }
    @Override
    public void dive() {
        System.out.println(this.toString() + "dive.");
        
    }
    @Override
    public void drive() {
        System.out.println(this.toString() + "drive.");
        
    }
    @Override
    public void brake() {
        System.out.println(this.toString() + "brake.");
        
    }
}
