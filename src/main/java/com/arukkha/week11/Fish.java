package com.arukkha.week11;

public class Fish extends Animal implements Swimable,Diveable{

    public Fish(String name) {
        super(name, 0);
    }
    @Override
    public void swim() {
        System.out.println(this.toString() + "swim.");
        
    }
    @Override
    public void dive() {
        System.out.println(this.toString() + "dive.");
        
    }
    @Override
    public void sleep() {
        System.out.println(this.toString() + "sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + "eat.");

    }
    @Override
    public String toString() {
        return "Rat ("+ this.getName() +")";
    }
}
