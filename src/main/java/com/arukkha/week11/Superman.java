package com.arukkha.week11;

public class Superman extends Human implements Flyable{

    public Superman(String name) {
        super(name);
    }

    @Override
    public void takeoff() {
        System.out.println(this.toString() + "takeoff.");

    }

    @Override
    public void fly() {
        System.out.println(this.toString() + "fly.");

    }

    @Override
    public void landing() {
        System.out.println(this.toString() + "landing.");

    }
    @Override
    public String toString() {
        return "Superman ("+ this.getName() +")";
    }
    @Override
    public void crawl() {
        System.out.println(this.toString() + "crawl.");
        
    }
    @Override
    public void dive() {
        System.out.println(this.toString() + "dive.");
        
    }
    
}
